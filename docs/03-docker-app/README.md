Simple dockerfile to build application , which show only one generated page

to show latest Demo App commit ID we are use the before_script Gitlab pipeline part

```
  before_script:
    - echo "<pre>" > $CI_PROJECT_DIR/nginx-app/index.html 
    - echo ${CI_COMMIT_MESSAGE} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_COMMIT_SHA} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_COMMIT_SHORT_SHA} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_SERVER_URL} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo "</pre>" >> $CI_PROJECT_DIR/nginx-app/index.html
```


and copy then index.html to webroot

```
COPY index.html /usr/share/nginx/html/
```

