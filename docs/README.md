# Table of content

1. [Overview](#overview)
2. [Installation](./01-installation)
3. [Basic usage](./02-basic-usage)
4. [Docker app](./03-docker-app)
5. [Gitlab build pipeline](./docs/04-gitlab)
5. [last checks](./05-last-checks)



# Overview

We need to have VM deployed with following workloads:
 * K8s (any distro) 
 * Argo CI (K8s based)
        Argo CI is not maintained anymore from the looks of it, but it should have been a CI tool that triggers workflows based on git changes.
 
 
 - Argo Cd (K8s based)
 - Argo CI pipeline for Demo App (run simplest unit test + build docker image for the Demo App)
 - Argo CD pipeline for Demo App Deployment
 - Demo App deployment (K8s based)
 As a result we should have at least one git repository which represents mentioned functionality 
 Notes:
 - VM should be deployed with vagrant
 - VM should be  configured with ansible 
 - Demo App is  Nginx with single HTML page which represents latest Demo App commit ID 
 - Argo CI pipeline may be started manually or automatically (for each new commit to Demo App repo)
 - Argo CD should be started automatically
 





