GitLab CI Pipeline

The next step is to create the pipeline, which will automatically build our application, 
push to the container registry and update Kubernetes manifests in appropriate environments.

to make sure the pipeline will work correctly we need to create an Access Token with API scope. 
This will be used later in the pipeline for git commits. 
You can create your own in USER -> SETTINGS -> ACCESS TOKENS

Next, add necessary Environment Variables to the project ( Settings -> CI/CD -> Variables):

    CI_PUSH_TOKEN — the token
    CI_USERNAME — the username of the token owner


Pipeline contain two stages - build nginx - build application from Dockerfile 
and dev_nginx - kustomize ArgoDC manifest to automaticaly update the deployment

```
stages:
  - build_nginx
  - dev_nginx


build_nginx:
  stage: build_nginx
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  before_script:
    - echo "<pre>" > $CI_PROJECT_DIR/nginx-app/index.html 
    - echo ${CI_COMMIT_MESSAGE} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_COMMIT_SHA} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_COMMIT_SHORT_SHA} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo ${CI_SERVER_URL} >> $CI_PROJECT_DIR/nginx-app/index.html
    - echo "</pre>" >> $CI_PROJECT_DIR/nginx-app/index.html
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/nginx-app --dockerfile $CI_PROJECT_DIR/nginx-app/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --label key=value
  only:
    - master

dev_nginx:
  stage: dev_nginx
  image: alpine:3.8
  before_script:
    - apk add --no-cache git curl bash
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
    - mv kustomize /usr/local/bin/
    - git remote set-url origin https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/shaposhnikoff/argo_k8s.git
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - git checkout -B master
    - cd deployment/dev
    - kustomize edit set image $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - cat kustomization.yaml
    - git commit -am '[skip ci] DEV image update'
    - git push origin master
  only:
    - master

```

