ArgoCD Application Setup

Its time to configure our applications in Kubernetes

```
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: web-app-dev
  namespace: argocd
spec:
  project: default
  source: 
    repoURL: https://gitlab.com/shaposhnikoff/argo_k8s.git
    targetRevision: HEAD
    path: deployment/dev
  destination:
    server: https://kubernetes.default.svc
    namespace: dev
  syncPolicy:
    automated:
      prune: true

```

The most important parts here are:

    name — the name of our Argo CD application
    namespace — must be the same as Argo CD instance
    project — project name where the application will be configured (this is the way to organize your applications in Argo CD)
    repoURL— URL of our source code repository
    targetRevision — the git branch you want to use
    path — the path where Kubernetes manifests are stored inside repository
    destination — Kubernetes destination-related things (in this case the cluster is the same where Argo CD is hosted)



Apply these manifest using kubectl ( kubectl apply -f ..)


