WebUI with installed application

<img src="./pics/webui_01.png" alt="drawing" width="800"/>

Detailed view

<img src="./pics/webui_02.png" alt="drawing" width="800"/>


Autosync enabled

<img src="./pics/autosync.png" alt="drawing" width="800"/>

Currently Argo CD has two modes for application's state syncing:

* auto-polling of the app repository
* webhook-triggered sync


In this lab i'm use only auto-polling of autosync ( 3 mins interval by default )


Official [doc](https://argoproj.github.io/argo-cd/operator-manual/webhook/) syncing the app state via webhook :)


```



max@max ~ $ kc get ing -n dev
NAME            HOSTS                       ADDRESS                                  PORTS   AGE
gitops-webapp   webapp.dev.k8s.local   172.18.8.101,172.18.8.102,172.18.8.103   80      6h26m
max@max-dacha ~ $
```

making ingress available localy 


```
# echo "172.18.8.101   webapp.dev.k8s.local" >> /etc/hosts
```

then 

```
max@max dev (master) $ curl webapp.dev.k8s.local
<pre>
2020-08-27-072231 I did it for the lulz!
9c0c73edd6759eb127fff746db05724f93a26315
9c0c73ed
https://gitlab.com
</pre>
max@max-dacha dev (master) $
max@max-dacha dev (master) $
```

or in firefox 

<img src="./pics/weapp_01.png" alt="drawing" width="800"/>

